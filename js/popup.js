(function($, window, document, Drupal) {

	function Popup() {
		this.timing = parseInt(drupalSettings.vb_popup.timing);
		this.id = drupalSettings.vb_popup.id;
		this.init();
	}

	Popup.prototype.init = function() {
		this.attachHandlers();
		if(!this.getCookie('popup_' + this.id)) {
			var popup = this;
			setTimeout(function() {
				popup.openPopup()
				popup.setCookie('popup_' + popup.id, 'watched', 1);
			}, popup.timing * 1000);
		}
	}

	Popup.prototype.setCookie = function(name, value, expires) {
		var d = new Date();
		d.setTime(d.getTime() + (expires*1*60*60*1000)); // hours * minutes * seconds * miliseconds
		var cookieExpires = "expires="+ d.toUTCString();
		document.cookie = name + "=" + value + ";" + cookieExpires + ";path=/";
	}
	Popup.prototype.getCookie = function(cookieName) {
		var name = cookieName + "=";
		var decodedCookie = document.cookie;
		var ca = decodedCookie.split(';');
		for(var i = 0; i <ca.length; i++) {
			var c = ca[i];
			while (c.charAt(0) == ' ') {
				c = c.substring(1);
			}
			if (c.indexOf(name) == 0) {
				return c.substring(name.length, c.length);
			}
		}
		return "";
	}

	Popup.prototype.attachHandlers = function() {
		var popup = this;
		$('.popup__close, .popup__mask').on('click', function(event) {
			event.preventDefault();
			popup.closePopup();
		});
	}

	Popup.prototype.openPopup = function() {
		$('.popup').addClass('popup--show');
	}
	Popup.prototype.closePopup = function() {
		$('.popup').removeClass('popup--show');
	}

	$(function() {
		var popup = new Popup;
	});

})(jQuery, window, document, Drupal);